package com.app.reparer.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.app.reparer.R;
import com.app.reparer.Volley.ApiCall;
import com.app.reparer.Volley.VolleyCallback;
import com.app.reparer.helpers.LocaleUtils;
import com.app.reparer.helpers.SharedHelper;
import com.app.reparer.helpers.UrlHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends BaseActivity {
    AppSettings appSettings = new AppSettings(SplashActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        LocaleUtils.setLocale(new Locale(Locale.FRENCH.getLanguage()));
        LocaleUtils.updateConfig(getApplication(),
                getBaseContext().getResources().getConfiguration());
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.app.reparer",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

//        ImageView spinningEarth = (ImageView)findViewById(R.id.bottomWorld);
//        RotateAnimation rotateAnimation = new RotateAnimation(360,0,
// Animation.RELATIVE_TO_SELF,.5f,Animation.RELATIVE_TO_SELF,.5f);
//        rotateAnimation .setDuration(50000);
//        rotateAnimation .setFillAfter(true);
//        rotateAnimation .setRepeatCount(-1);
//        CycleInterpolator interpolator= new CycleInterpolator(2);
//        rotateAnimation.setInterpolator(new CycleInterpolator(2));
//
//        spinningEarth.setAnimation(rotateAnimation);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (appSettings.getIsLogged().equalsIgnoreCase("true")) {
                    upDateToken();

                    moveMainActivity();

                } else {
                    moveOnBoardActivity();

                }
            }
        }, 3000);
    }

    private void upDateToken() {
        ApiCall.PostMethodHeaders(SplashActivity.this, UrlHelper.UPDATE_DEVICE_TOKEN, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
            }
        });
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fcm_token", appSettings.getFireBaseToken());
            jsonObject.put("os", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private void moveOnBoardActivity() {
        Intent onboard = new Intent(SplashActivity.this, OnboardActivity.class);
        SharedHelper.putKey(SplashActivity.this, "language_change", "fr");
        LocaleUtils.setLocale(new Locale(Locale.FRENCH.getLanguage()));
        LocaleUtils.updateConfig(getApplication(),
                getBaseContext().getResources().getConfiguration());
        startActivity(onboard);
        finish();
    }

    private void moveMainActivity() {
        /*Intent intent = new Intent(SplashActivity.this, MainActivity.class);

        startActivity(intent);
        finish();*/

        String name = SharedHelper.getKey(SplashActivity.this, "language_change");
        switch (name) {
            /*case "en": {
                SharedHelper.putKey(SplashActivity.this, "language_change", "en");
                Intent intent2 = new Intent(SplashActivity.this, MainActivity.class);
                intent2.putExtra("type", "new");
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                LocaleUtils.setLocale(new Locale(Locale.ENGLISH.getLanguage()));
                LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                break;
            }*/
            case "fr": {
                SharedHelper.putKey(SplashActivity.this, "language_change", "fr");
                Intent intent2 = new Intent(SplashActivity.this, MainActivity.class);
                intent2.putExtra("type", "new");
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                LocaleUtils.setLocale(new Locale(Locale.FRENCH.getLanguage()));
                LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                break;
            }
            default: {
                SharedHelper.putKey(SplashActivity.this, "language_change", "fr");
                Intent intent2 = new Intent(SplashActivity.this, MainActivity.class);
                intent2.putExtra("type", "new");
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                LocaleUtils.setLocale(new Locale(Locale.FRENCH.getLanguage()));
                LocaleUtils.updateConfig(getApplication(), getBaseContext().getResources().getConfiguration());
                startActivity(intent2);
                break;
            }
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
