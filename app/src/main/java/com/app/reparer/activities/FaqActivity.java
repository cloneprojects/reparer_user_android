package com.app.reparer.activities;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.app.reparer.R;
import com.app.reparer.Volley.ApiCall;
import com.app.reparer.Volley.VolleyCallback;
import com.app.reparer.helpers.LocaleUtils;
import com.app.reparer.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

public class FaqActivity extends BaseActivity {

    private WebView webView;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        LocaleUtils.setLocale(new Locale(Locale.FRENCH.getLanguage()));
        LocaleUtils.updateConfig(getApplication(),
                getBaseContext().getResources().getConfiguration());
        webView = (WebView) findViewById(R.id.webView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ApiCall.getMethod(FaqActivity.this, UrlHelper.FAQ, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray static_pages = response.optJSONArray("static_pages");
                JSONObject jsonObject = new JSONObject();

                String url = jsonObject.optString("page_url");
                webView.getSettings().setJavaScriptEnabled(false);
                webView.loadUrl(url);
                webView.setHorizontalScrollBarEnabled(false);
            }
        });

    }
}
