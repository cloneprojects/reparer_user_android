package com.app.reparer.activities;

import android.support.v7.app.AppCompatActivity;

import com.app.reparer.helpers.LocaleUtils;

public class BaseActivity extends AppCompatActivity {

    public BaseActivity() {
        LocaleUtils.updateConfig(this);
    }
} 